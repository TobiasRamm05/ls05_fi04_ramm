public class Aufgabe2 {
public static void main(String[] args) {
		
	System.out.printf("%-5s = %-18s = %4d \n", "0!", "",1);
	System.out.printf("%-5s = %-18s = %4d \n", "1!", "1",1);
	System.out.printf("%-5s = %-18s = %4d \n", "2!", "1 * 2",1*2);
	System.out.printf("%-5s = %-18s = %4d \n", "3!", "1 * 2 * 3",1*2*3);
	System.out.printf("%-5s = %-18s = %4d \n", "4!", "1 * 2 * 3 * 4",1*2*3*4);
	System.out.printf("%-5s = %-18s = %4d \n", "5!", "1 * 2 * 3 * 4 * 5",1*2*3*4*5);

	// % ist linksb�ndig
	//%4d steht f�r rechtsb�ndig
	//"%-5s = %-18s = %4d \n" <-- Strucktur, "0!" <-- Aufgabe, "" <--Ausgabe der rechnung,1 <-- Rechnung
	}

}
